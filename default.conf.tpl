# NGINX proxy template file ...
server {
  listen ${LISTEN_PORT};

  # Django configuration for static requests
  location /static {
    alias /vol/static;
  }

  # All other requests
  location / {
    # uWSGI service running python application
    uwsgi_pass           ${APP_HOST}:${APP_PORT};
    include              /etc/nginx/uwsgi_params;
    client_max_body_size 10M;
  }
}
