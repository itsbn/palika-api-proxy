# Palika application API proxy
Nginx reverse proxy for load balancer and serving static contents

## Usage

### Environment Variables

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname of the app to forward requests to (default: `coreapp`)
* `APP_PORT` - Port of the app to forward requests to (default: `9000`)
